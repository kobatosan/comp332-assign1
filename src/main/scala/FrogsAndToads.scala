// Anisha Joshni Jose 45229058 Submitted: 3/9/19

/*
 * This file is part of COMP332 Assignment 1.
 *
 * Copyright (C) 2019 Dominic Verity, Macquarie University.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package org.mq.frogsandtoads

import doodle.core._
import doodle.syntax._
import doodle.image.Image._
import doodle.image.Image

/**
  * A puzzle state is given as a 1-dimensional array of cell values.
  */
class PuzzleState private (
    board: Vector[PuzzleState.Cell],
    loc: Int
) {

  import PuzzleState._

  val size = board.size
  val emptyLoc = loc

  def isTerminalState(): Boolean = {
    board.slice(0, emptyLoc).forall(_ == Toad) &&
    board(emptyLoc) == Empty &&
    board.slice(emptyLoc + 1, size).forall(_ == Frog)
  }

  def isInitialState(): Boolean = {
    board.slice(0, emptyLoc).forall(_ == Frog) &&
    board(emptyLoc) == Empty &&
    board.slice(emptyLoc + 1, size).forall(_ == Toad)
  }

  def renderState(): Image = {
    board.map(x => renderCell(x))
    .reduce((x,y) => x beside y) 

  }

  override def toString() =
    "[" ++ board.map(_.toString).reduceLeft(_ ++ "|" ++ _) + "]"


  def slideRight() : Option[PuzzleState] = {
    if((emptyLoc-1 >= 0)) {
      if(board(emptyLoc-1) == Frog) {
      // board.slide(0, emptyLoc-1) // inclusive, exclusive (-2)
      val move = board.slice(0, emptyLoc-1) ++ Vector(Empty, Frog) ++ board.slice(emptyLoc+1, size)
      Some(new PuzzleState(move, emptyLoc-1))
      
      }

      else None

    } // end first if

    else None 

  }

  def slideLeft() : Option[PuzzleState] = {
    if((emptyLoc+1 < size)) { 
      if( board(emptyLoc+1) == Toad) {
      val move = board.slice(0, emptyLoc) ++ Vector(Toad, Empty) ++ board.slice(emptyLoc+2, size)
      Some(new PuzzleState(move, emptyLoc+1))

      }

      else None

    } // end first if

    else None 

  }

  def leapRight() : Option[PuzzleState] = {
    if((emptyLoc-2 >= 0)) {
      if(board(emptyLoc-2) == Frog && board(emptyLoc-1) == Toad) {
      val move = board.slice(0, emptyLoc-2) ++ Vector(Empty, Toad, Frog) ++ board.slice(emptyLoc+1, size)
      Some(new PuzzleState(move, emptyLoc-2))

      }

      else None

    } // end first if

    else None

  }

  def leapLeft() : Option[PuzzleState] = {
    if((emptyLoc+2 < size)) {
      if(board(emptyLoc+2) == Toad && board(emptyLoc+1) == Frog) {
      val move = board.slice(0, emptyLoc) ++ Vector(Toad, Frog, Empty) ++ board.slice(emptyLoc+3, size)
      Some(new PuzzleState(move, emptyLoc+2))

      }

      else None

    } // end first if

    else None

  }


} // end private PuzzleState


/**
  * Companion object for the [[PuzzleState]] class, provides a public constructor.
  */
object PuzzleState {
  import scala.util.matching.Regex

  /**
    * Case class for case objects to represent the possible contents of a
    * cell in the puzzle.
    */
  sealed abstract class Cell {
  override def toString() : String = 
    this match {
      case Frog => "F"
      case Toad => "T"
      case Empty => " " 
  }

}
  
  case object Frog extends Cell
  case object Toad extends Cell
  case object Empty extends Cell

  /**
    * Construct a [[PuzzleState]] object in the initial state for a
    * puzzle with specified numbers of frogs and toads.
    *
    * @param frogs number of frogs to place on the left of the [[PuzzleState]]
    * to be constructed
    * @param toads number of toads top place on the right of the [[PuzzleState]]
    * to be constructed
    */
  def apply(frogs: Int, toads: Int): PuzzleState = {
    if (frogs <= 0 || frogs > 10)
      throw new Exception("The number of frogs must be between 1 and 10.")

    if (toads <= 0 || toads > 10)
      throw new Exception("The number of frogs must be between 1 and 10.")

    new PuzzleState(
      Vector.fill(frogs)(Frog) ++ Vector(Empty) ++
        Vector.fill(toads)(Toad),
      frogs
    )
  }

 /*
   * Regular expression for stripping a pair of square brackets off
   * of either end of a string.
   */
  val brackReg: Regex = """^\s*\[(.*)\]\s*$""".r

  /**
    * Construct a [[PuzzleState]] object by parsing a string representation
    * consisting of the symbols `F`, `T` and `<space>` separated by pipes `|`
    * and surrounded by square brackets.
    *
    * This must contain at least one `F`, at least one `T` and exactly one
    * `<space>`, otherwise an exception is raised.
    *
    * This apply method is inverse, in the manifest sese, to the pretty
    * printing [[PuzzleState#toString]] method.
    *
    * @param str the string to be parsed into a [[PuzzleState]] object.
    */
  def apply(str: String): PuzzleState = {
    val brackReg(s) = str
    val board: Vector[Cell] = s
      .split('|')
      .map(
        s =>
          s.trim match {
            case "F" => Frog
            case "T" => Toad
            case ""  => Empty
            case s =>
              throw new Exception("Unexpected cell contents '" + s + "'.")
          }
      )
      .toVector

    if (board.count(_ == Frog) < 1)
      throw new Exception("A puzzle state must have at least one frog.")

    if (board.count(_ == Toad) < 1)
      throw new Exception("A puzzle state must have at least one toad.")

    if (board.count(_ == Empty) != 1)
      throw new Exception("A puzzle state must have one empty cell.")

    new PuzzleState(board, board.indexOf(Empty))

  }


  /**
    * Find a sequence of legal moves of the frogs and toads puzzle from a specified starting
    * [[PuzzleState]] to the terminal [[PuzzleState]].
    *
    * @param start the starting [[PuzzleState]]
    * @return the sequence of [[PuzzleState]] objects passed through in the transit from
    * state `start` to the terminal state (inclusive). Returns the empty sequence if no solution
    * is found.
    */
  def solve(start: PuzzleState): Seq[PuzzleState] = {
    val sequence = Seq[PuzzleState](start)

    if(start.isTerminalState) {
      sequence 
    }

    else {
     val moves = Vector(start.leapRight, start.slideRight, start.slideLeft, start.leapLeft)
      .filter(_.nonEmpty).map(x => solve(x.get))
      .filter(x => x.nonEmpty && x.last.isTerminalState)

      if(moves.isEmpty) {
        Seq()

      }

      else return sequence ++ moves.head
    
    } // end first else

  } // end solve

 def renderCell(theCell : Cell) : Image = 
    theCell match {
     case Frog => circle(60).fillColor(Color.blue)
     case Toad => circle(60).fillColor(Color.lightBlue)
     case Empty => circle(60).fillColor(Color.white) 

 }
  
  /**
    * Call [[solve]] to generate a sequence of legal moves from a specified
    * starting [[PuzzleState]] to the terminal [[PuzzleState]]. Render each state in that solution as
    * an image and return the resulting sequence of images.
    *
    * @param start the starting [[PuzzleState]]
    * @return the sequence of [[Image]] objects depicting the sequence of puzzle states
    * passed through in the transit from the `start` state to the terminal state.
    */

 def animate(start: PuzzleState): Seq[Image] = {
    solve(start)
    .map(_.renderState)

 }

  /**
    * Create an animation of a solution to the frogs and toads puzzle, starting from the initial
    * [[PuzzleState]] and ending at the terminal [[PuzzleState]].
    * 
    * @param frogs the number of frogs in the puzzle (between 1 and 10 inclusive)
    * @param toads the number of toads in the puzzle (between 1 and 10 inclusive)
    */
  def animate(frogs: Int, toads: Int): Seq[Image] =
    animate(PuzzleState(frogs, toads))

    
} // end public PuzzleState
