// Anisha Joshni Jose 45229058 Submitted: 3/9/19

/*
 * This file is part of COMP332 Assignment 1.
 *
 * Copyright (C) 2019 Dominic Verity, Macquarie University.
 *
 * This Source Code Form is subject to the terms of the Mozilla Public
 * License, v. 2.0. If a copy of the MPL was not distributed with this
 * file, You can obtain one at http://mozilla.org/MPL/2.0/.
 *
 * Tests of the Frogs and Toads puzzle solver.
 * Uses the ScalaTest `FlatSpec` style for writing tests. See
 *
 *      http://www.scalatest.org/user_guide
 *
 * For more info on writing ScalaTest tests.
 */

package org.mq.frogsandtoads

import org.scalatest.FlatSpec
import org.scalatest.Matchers

class FrogsAndToadsTests extends FlatSpec with Matchers {

  import PuzzleState._

  "A puzzle state with 5 frogs and 8 toads:" should
    "have 14 cells including empty cell" in {
    assert(PuzzleState(5, 8).size == 14)
  }

  it should "have its empty cell at position 5" in {
    assertResult(5) {
      PuzzleState(5, 8).emptyLoc
    }
  }

  it should "be constructed in the initial puzzle state" in {
    assert(PuzzleState(5, 8).isInitialState())
  }

  it should "not be constructed in the terminal puzzle state" in {
    assert(!PuzzleState(5, 8).isTerminalState())
  }


  "A puzzle state with 2 frogs and 2 toads:" should
    "have 5 cells including empty cell" in {
    assert(PuzzleState(2, 2).size == 5)
  }

 it should "be out of bounds (illegal) when slideRight from the state [ |F|F|T|T]" in {
  assertResult(None) {
    PuzzleState("[ |T|T|F|F]").slideRight()
  }
 }

 it should "check valid frog and leapLeft from the state [F|T| |F|T]" in {
  assertResult(Some("[F|T|T|F| ]")) {
    PuzzleState("[F|T| |F|T]").leapLeft().map(_.toString())
  }
 }

 it should "not leapLeft (illegal) from the state [F|T| |T|F]" in {
  assertResult(None) {
    PuzzleState("[F|T| |T|F]").leapLeft()
  }
 }


  "A puzzle state with 3 frogs and 1 toad:" should
    "have 5 cells including empty cell" in {
    assert(PuzzleState(2, 2).size == 5)
  }

 it should "slideRight from the state [F|F|F| |T]" in {
  assertResult(Some("[F|F| |F|T]")) {
    PuzzleState("[F|F|F| |T]").slideRight().map(_.toString())
  }
 }

 it should "not slideLeft (illegal) from the state [F|F| |F|T]" in {
  assertResult(None) {
    PuzzleState("[F|F| |F|T]").slideLeft()
  }
 }

 it should "not leapRight (illegal) from the state [F| |T|F|F]" in {
  assertResult(None) {
    PuzzleState("[F| |T|F|F]").leapRight()
  }
 }

 it should "leapRight from the state [F|T| |F|F]" in {
  assertResult(Some("[ |T|F|F|F]")) {
    PuzzleState("[F|T| |F|F]").leapRight().map(_.toString())
  }
 }


 "A puzzle state with 1 frog and 1 toad:" should
    "have 3 cells including empty cell" in {
    assert(PuzzleState(1, 1).size == 3)
  }

 it should "be out of bounds (illegal) when leapLeft from the state [T| |F]" in {
  assertResult(None) {
    PuzzleState("[T| |F]").leapLeft()
  }
 }

 it should "slideRight from the state [F| |T]" in {
  assertResult(Some("[ |F|T]")) {
    PuzzleState("[F| |T]").slideRight().map(_.toString())
  }
 }


 "A puzzle state with 1 frog and 2 toads:" should
    "have 4 cells including empty cell" in {
    assert(PuzzleState(1, 2).size == 4)
  }

 it should "be out of bounds (illegal) when slideLeft from the state [T|T|F| ]" in {
  assertResult(None) {
    PuzzleState("[T|T|F| ]").slideLeft()
  }
 }


 "A puzzle state with 2 frogs and 3 toads:" should
    "have 6 cells including empty cell" in {
    assert(PuzzleState(2, 3).size == 6)
  }

 it should "slideLeft from the state [F|F|T|T| |T]" in {
  assertResult(Some("[F|T|T|T| ]")) {
    PuzzleState("[F|T|T| |T]").slideLeft().map(_.toString())
  }
 }


 "A puzzle state with 2 frogs and 1 toad:" should
    "have 4 cells including empty cell" in {
    assert(PuzzleState(2, 1).size == 4)
  }

 it should "be out of bounds (illegal) when leapRight from the state [ |T|F|F]" in {
  assertResult(None) {
    PuzzleState("[ |T|F|F]").leapRight()
  }
 }


} // end FrogsAndToadsTests class
